# VT  

## `gulp` コマンドでローカルテスト  

PNG画像圧縮とSCSS、EJSのコンパイル。あとウォッチャー  

## `build` コマンドでbuild  

こちらはbrowser-syncがない  

## main  

```
run build  
cd dist  
dist/main.html  
```

## close form.ejsとcampaign.ejs、form.scssを差し替える  

```
run build  
cd dist  
dist/close.html  
```

## campaign campaign.ejsを消し、form.ejs、agree.ejs、footer.ejsとform.scss、footer.scssを差し替える  

```
run build  
cd dist  
dist/campaign.html  
```
