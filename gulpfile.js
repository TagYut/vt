var gulp = require('gulp');
var ejs = require('gulp-ejs');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var imageminPng = require('imagemin-pngquant');
var changed  = require('gulp-changed');
var browserSync = require('browser-sync');

//  test command
//  ejs to HTML
gulp.task('ejs', function() {
    gulp.src(
        ['src/ejs/*.ejs','!' + 'src/ejs/_*.ejs']
    )
        .pipe(plumber())
        .pipe(ejs())
        .pipe(rename({extname: '.html'}))
        .pipe(gulp.dest('dist'));
});

//  ejs watcher
gulp.task('ejs-watch', ['ejs'], function(){
        var ejswatcher = gulp.watch('src/ejs/**/*.ejs', ['ejs']);
        ejswatcher.on('change', function(event) {
    });
});

//  sass to css
gulp.task('sass', function() {
    gulp.src('src/sass/*.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(gulp.dest('dist/css'));
});

//  sass watcher
gulp.task('sass-watch', ['sass'], function(){
    var sasswatcher = gulp.watch('src/sass/**/*.scss', ['sass']);
    sasswatcher.on('change', function(event) {
    });
});

//  imagemin png
gulp.task('imagemin', function() {
    gulp.src('src/img/*.png')
    .pipe(changed('dist/img'))
    .pipe(imagemin([imageminPng()]))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('image-watch', ['imagemin'], function(){
    var imagewatcher = gulp.watch('src/img/*.png', ['imagemin']);
    imagewatcher.on('change', function(event) {
    });
});

//  browser-sync
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "dist",
            index  : "main.html"
        }
    });
});

gulp.task('default', ['sass-watch','ejs-watch','image-watch','browser-sync']);
gulp.task('build', ['sass','ejs','imagemin']);
